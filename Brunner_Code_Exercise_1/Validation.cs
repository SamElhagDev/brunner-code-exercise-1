﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Brunner_Code_Exercise_1
{
    class Validation
    {
        public static List<String> ValidationController()
        {
            string registerInput = Console.ReadLine();
            string[] shoppingArray = registerInput.Replace('[', ' ').Replace(']', ' ').Trim().Split(',');
            List<String> shoppingList = shoppingArray.ToList();
            Console.ReadLine();
            return shoppingList;
        }
    }
}
